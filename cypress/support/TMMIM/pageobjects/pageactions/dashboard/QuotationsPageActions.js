/// <reference types="cypress" />

import QuotationsPageElements from '../../pageelements/dashboard/QuotationsPageElements'
import CommonPageElements from '../../pageelements/common/CommonPageElements'

export default class QuotationsPageActions{
    constructor(){

        globalThis.quotationsPageElements = new QuotationsPageElements()
        globalThis.commonPageElements = new CommonPageElements()
    }

    SearchRFQsAndClick(RFQsObj){
      
        cy.wait(3000)
        cy.get('tr td').each(($el,index, $list)=> {

            let infoText = $el.text();
             
            if(infoText.includes(RFQsObj['RFQsNo'])){
               cy.get("tr td").eq(index).click()
            }
        })
        cy.wait(2000)
    }


    ResubmitQuotation(QuotationsObj){
        quotationsPageElements.getQuotationAmount()
            .focus()
            .clear()
            .type(QuotationsObj['quotationAmount'])
            .wait(1000)

        quotationsPageElements.getQuotationDate()
            .focus()
            .clear()
            .type(QuotationsObj['quotationDate'])
            .wait(1000)

        //press Enter
        quotationsPageElements.getQuotationDate().type('{enter}').wait(1000)   

        quotationsPageElements.getQuotationStartTime()
            .focus()
            .clear()
            .type(QuotationsObj['quotationStartTime'])
            .wait(1000)

        //press Enter
        quotationsPageElements.getQuotationStartTime().type('{enter}').wait(1000)    

        quotationsPageElements.getQuotationEndTime()
            .focus()
            .clear()
            .type(QuotationsObj['quotationEndTime'])
            .wait(1000)

        //press Enter
        quotationsPageElements.getQuotationEndTime().type('{enter}').wait(1000)       

        quotationsPageElements.getQuotationMessage()
            .focus()
            .clear()
            .type(QuotationsObj['message'])
            .wait(1000)

        //Resubmit    
        quotationsPageElements.getReSubmitButton().should('contain', QuotationsObj['resubmitButtonName']).click()

    }


    //cypress file download only support https url
    DownloadQuotationCSVFile(QuotationsObj){
        cy.downloadFile(QuotationsObj['downloadLink'],'downloads','Qutation-report.csv')

    }

    ClickDownloadQuotationCSVFile(QuotationsObj){
        quotationsPageElements.getdownloadButton().should('contain', QuotationsObj['downloadButtonName']).click()

    }

    QuotationSelectOptionbyName(option){
        commonPageElements.getSelectOptionbyName().eq(1).select(option)
    }

}