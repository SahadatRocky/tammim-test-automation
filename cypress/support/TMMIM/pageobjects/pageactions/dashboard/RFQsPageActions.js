/// <reference types="cypress" />
import RFQsPageElements from '../../pageelements/dashboard/RFQsPageElements'
import CommonPageElements from '../../pageelements/common/CommonPageElements'
export default class RFQsPageActions{
    constructor(){

        globalThis.rfqsPageElements = new RFQsPageElements()
        globalThis.commonPageElements = new CommonPageElements()
    }

    SearchRFQsNoAndClick(RFQsObj){
        cy.wait(3000)
        cy.get('tr td').each(($el,index, $list)=> {
            let infoText = $el.text();
            if(infoText.includes(RFQsObj['RFQsNo'])){

               cy.get("tr td").eq(index).click()
            }
        })
        cy.wait(2000)
    }


    MakeAQuotationButton(RFQsObj){
        rfqsPageElements.getMakeAQuotationButton().should('contain', RFQsObj['QuotationButton']).click().wait(2000)

        rfqsPageElements.getMakeQuotationAmount()
            .focus()
            .clear()
            .type(RFQsObj['quotationAmount'])
            .wait(1000)

        rfqsPageElements.getMakeQuotationDate()
            .focus()
            .clear()
            .type(RFQsObj['quotationDate'])
            .wait(1000)

        //press Enter
        rfqsPageElements.getMakeQuotationDate().type('{enter}').wait(1000)   

        rfqsPageElements.getMakeQuotationStartTime()
            .focus()
            .clear()
            .type(RFQsObj['quotationStartTime'])
            .wait(1000)

        //press Enter
        rfqsPageElements.getMakeQuotationStartTime().type('{enter}').wait(1000)    

        rfqsPageElements.getMakeQuotationEndTime()
            .focus()
            .clear()
            .type(RFQsObj['quotationEndTime'])
            .wait(1000)

        //press Enter
        rfqsPageElements.getMakeQuotationEndTime().type('{enter}').wait(1000)       

        rfqsPageElements.getMakeQuotationMessage()()
            .focus()
            .clear()
            .type(RFQsObj['message'])
            .wait(1000)

         //choose image 
         rfqsPageElements.getMakeChooseFile()
         .eq(0)
         .attachFile(RFQsObj['imageFileName'])
         .wait(1000)   

        //submit    
        rfqsPageElements.getMakeSubmitButton().should('contain', QuotationsObj['submitButtonName']).click().wait(2000)
        

    }

    DeclineButton(RFQsObj){
        rfqsPageElements.getDeclineButton().should('contain', RFQsObj['DeclineButton']).click().wait(1000)
    }

    RFQsSelectOptionbyName(option){
        commonPageElements.getSelectOptionbyName().eq(1).select(option)
    }


}