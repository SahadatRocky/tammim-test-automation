/// <reference types="cypress" />
import SubscriptionPageElements from '../../pageelements/dashboard/SubscriptionPageElements'
import CommonPageElements from '../../pageelements/common/CommonPageElements'
export default class SubscriptionPageActions{
    
    constructor(){

        globalThis.subscriptionPageElements = new SubscriptionPageElements()
        globalThis.commonPageElements = new CommonPageElements()
    }


    subscribePlan(subscribeObj){

        subscriptionPageElements.getPlanField()
        .focus()
        .clear()
        .type(subscribeObj['plan']).wait(1000)
        //press Enter
        subscriptionPageElements.getPlanField().type('{enter}').wait(1000)

        subscriptionPageElements.getProcessButton()
            .should('contain', subscribeObj['ProceedButtonName'])
            .click()
            .wait(2000) 

        subscriptionPageElements.getCardNumberField()
            .focus()
            .clear()
            .type(subscribeObj['cardNumber'])
            .wait(1000)

        subscriptionPageElements.getExpireMonthField()    
            .focus()
            .clear()
            .type(subscribeObj['expMonth'])
            .wait(1000)
        
        subscriptionPageElements.getExpireYearField()    
            .focus()
            .clear()
            .type(subscribeObj['expYear'])
            .wait(1000)
            
            
        subscriptionPageElements.getCVVField()    
            .focus()
            .clear()
            .type(subscribeObj['ccv'])
            .wait(1000)

        subscriptionPageElements.getPayNowButton()
            .should('contain', subscribeObj['payNowButtonName'])
            .click()
            .wait(2000)

        subscriptionPageElements.getAuthenticateButton().click({force:true}).wait(2000)

        subscriptionPageElements.getContinueButton().should('contain', subscribeObj['continue']).click().wait(2000)
    }

    
    checkPaymentHistory(paymentHistoryObj){

        cy.get(".ant-table-tbody")
                    .eq(0)
                    .find(".ant-table-tbody > :nth-child(1) > :nth-child(1)")
                    .contains(paymentHistoryObj['paymentDate'])
                    .find(".ant-table-tbody > :nth-child(1) > :nth-child(2)")
                    .contains(paymentHistoryObj['subscriptionPlan'])
                    .find(".ant-table-tbody > :nth-child(1) > :nth-child(5)")
                    .contains(paymentHistoryObj['amount'])


        // cy.get('.ant-table-tbody').each(($el,index, $list)=> {
        //     let employeeNameText = $el.text();
        //     cy.log(employeeNameText)
        //     if(employeeNameText.includes(paymentHistoryObj['subscriptionPlan'])){
        //        cy.get(".ant-table-tbody")
        //             .eq(index)
        //             .find(".ant-table-tbody > :nth-child(1) > :nth-child(1)")
        //             .contains(paymentHistoryObj['paymentDate'])
        //             .find(".ant-table-tbody > :nth-child(1) > :nth-child(2)")
        //             .contains(paymentHistoryObj['subscriptionPlan'])
        //             .find(".ant-table-tbody > :nth-child(1) > :nth-child(5)")
        //             .contains(paymentHistoryObj['amount'])
      
        //        return false   
        //     }
        // })
        cy.wait(2000)
    }


}