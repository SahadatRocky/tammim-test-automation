/// <reference types="cypress" />
import ReservationPageElements from '../../pageelements/dashboard/ReservationPageElements'
import CommonPageElements from '../../pageelements/common/CommonPageElements'
export default class ReservationPageActions{
    constructor(){

        globalThis.reservationPageElements = new ReservationPageElements()
        globalThis.commonPageElements = new CommonPageElements()
    }

    SearchRFQsWithSpecificDateAndClicked(calanderViewObj){
        cy.wait(3000)
        cy.get('.rbc-calendar a').each(($el,index, $list)=> {
            let infoText = $el.text();
            if(infoText.includes(calanderViewObj['day'])){
                cy.log(infoText)
                if(calanderViewObj['FindRFQs']){
                    cy.get('.rbc-event > .rbc-event-content').contains(calanderViewObj['RFQsNo']).click({force:true}).wait(1000)
                    reservationPageElements.getButtonClose().click({force:true}).wait(1000)
                }else{
                    cy.log("ni")
                    cy.get('.rbc-date-cell').eq(infoText).click({force:true}).wait(1000)
                }
                return false;
            }
        })
        cy.wait(2000)
    }

    ClickDownloadReservationCSVFile(ListViewObj){
        reservationPageElements.getdownloadButton().should('contain', ListViewObj['downloadButtonName']).click()

    }

    CalenderViewCreateReservation(createReservationObj){

        reservationPageElements.getNameField()
            .focus()
            .clear()
            .type(createReservationObj['name'])
            .wait(1000)
   
        reservationPageElements.getEmailField()
            .focus()
            .clear()
            .type(createReservationObj['email'])
            .wait(1000)
   
        reservationPageElements.getPhoneNumberField()
            .focus()
            .clear()
            .type(createReservationObj['phone'])
            .wait(1000)
   
        // reservationPageElements.getDateField()
        //     .focus()
        //     .clear()
        //     .type(listViewObj['date'])
        //     .wait(1000)

        // //press Enter
        // reservationPageElements.getDateField().type('{enter}').wait(1000)
   
        reservationPageElements.getStartTimeField()
            .focus()
            .clear()
            .type(createReservationObj['startTime'])
            .wait(1000)

        //press Enter
        reservationPageElements.getStartTimeField().type('{enter}').wait(1000)
   
        reservationPageElements.getEndTimeField()
            .focus()
            .clear()
            .type(createReservationObj['endTime'])
            .wait(1000)

        //press Enter
        reservationPageElements.getEndTimeField().type('{enter}').wait(1000)
   
        reservationPageElements.getAllDayField().click()
   
        reservationPageElements.getDepositAmountField()
            .focus()
            .clear()
            .type(createReservationObj['depositAmount'])
            .wait(1000)
   
        reservationPageElements.getTotalField()
            .focus()
            .clear()
            .type(createReservationObj['total'])
            .wait(1000)
   
        reservationPageElements.getCreateButton().should('contain', createReservationObj['createButtonName']).click().wait(1000)
        reservationPageElements.getContinueButton().should('contain', createReservationObj['continueButtonName']).click().wait(1000)

    }

    CreateNewReservationButton(listViewObj){
        reservationPageElements.getCreateNewReservationButton().should('contain', listViewObj['createReservationButton']).click().wait(1000)
    }
    

    ReservationSelectOptionbyName(option){
        commonPageElements.getSelectOptionbyName().eq(1).select(option)
    }

    
    ListViewCreateReservation(createReservationObj){
        
        reservationPageElements.getNameField()
            .focus()
            .clear()
            .type(createReservationObj['name'])
            .wait(1000)
   
        reservationPageElements.getEmailField()
            .focus()
            .clear()
            .type(createReservationObj['email'])
            .wait(1000)
   
        reservationPageElements.getPhoneNumberField()
            .focus()
            .clear()
            .type(createReservationObj['phone'])
            .wait(1000)
   
        reservationPageElements.getDateField()
            .focus()
            .clear()
            .type(createReservationObj['date'])
            .wait(1000)

        //press Enter
        reservationPageElements.getDateField().type('{enter}').wait(1000)
   
        reservationPageElements.getStartTimeField()
            .focus()
            .clear()
            .type(createReservationObj['startTime'])
            .wait(1000)

        //press Enter
        reservationPageElements.getStartTimeField().type('{enter}').wait(1000)
   
        reservationPageElements.getEndTimeField()
            .focus()
            .clear()
            .type(createReservationObj['endTime'])
            .wait(1000)

        //press Enter
        reservationPageElements.getEndTimeField().type('{enter}').wait(1000)
   
        reservationPageElements.getAllDayField().click()
   
        reservationPageElements.getDepositAmountField()
            .focus()
            .clear()
            .type(createReservationObj['depositAmount'])
            .wait(1000)
   
        reservationPageElements.getTotalField()
            .focus()
            .clear()
            .type(createReservationObj['total'])
            .wait(1000)
   
       reservationPageElements.getCreateButton().should('contain', createReservationObj['createButtonName']).click()
       reservationPageElements.getContinueButton().should('contain', createReservationObj['continueButtonName']).click().wait(1000)

    }

}