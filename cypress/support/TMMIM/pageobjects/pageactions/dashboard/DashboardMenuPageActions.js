/// <reference types="cypress" />
import DashboardMenuPageElements from '../../pageelements/dashboard/DashboardMenuPageElements'

export default class DashboardMenuPageActions{
    
    constructor(){

        globalThis.dashboardMenuPageElements = new DashboardMenuPageElements()
    }

    onClickMenuDashboard(){
        dashboardMenuPageElements.getDashboard().should('contain', "Dashboard").click()    
    }

    onClickMenuSubscriptionSubscribe(){
        dashboardMenuPageElements.getSubscription()
            .should('contain', "Subscription")
            .wait(1000)
            .click()
            .then(() => {
                dashboardMenuPageElements.getSubscribe()
                .should('contain', "Subscribe")
                .click()
                .wait(1000)   
            })    
    }

    onClickMenuSubscriptionPaymentHistory(){
        dashboardMenuPageElements.getSubscription()
            .should('contain', "Subscription")
            .wait(1000)
            .click()
            .then(() => {
                dashboardMenuPageElements.getPaymentHistory()
                .should('contain', "Payment History")
                .click()
                .wait(1000)  
            })
    }


    onClickMenuManageProfileUpdateDetails(){
        dashboardMenuPageElements.getManageProfile()
            .should('contain', "Profile")
            .wait(1000)
            .click()
            .then(() => {
                dashboardMenuPageElements.getUpdateDetails()
                .should('contain', "Update Details")
                .click()
                .wait(1000)
            })
    }


    onClickMenuManageProfileUpdatePassword(){
        dashboardMenuPageElements.getManageProfile()
            .should('contain', "Profile")
            .wait(1000)
            .click()
            .then(() => {
                dashboardMenuPageElements.getUpdatePassword()
                .should('contain', "Update Password")
                .click()
                .wait(1000)
            })
       
    }

    onClickMenuReservation(){
        dashboardMenuPageElements.getReservation().should('contain', "Reservation").click()    
    }

    onClickMenuQuotations(){
        dashboardMenuPageElements.getQuotations().should('contain', "Quotations").click()    
    }

    onClickMenuRFQs(){
        dashboardMenuPageElements.getRFQs().should('contain', "RFQs").click()    
    }

    onClickMenuReview(){
        dashboardMenuPageElements.getReview().should('contain', "Review").click()    
    }

}