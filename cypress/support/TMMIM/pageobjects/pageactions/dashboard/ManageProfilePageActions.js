/// <reference types="cypress" />
import ManageProfilePageElements from '../../pageelements/dashboard/ManageProfilePageElements'

export default class ManageProfilePageActions{
    
    constructor(){

        globalThis.manageProfilePageElements = new ManageProfilePageElements()
    }


    updateProfileDetails(updateDetailsObj){
       // cy.scrollTo('top')    

        manageProfilePageElements.getCompanyNameField()
            .focus()
            .clear()
            .type(updateDetailsObj['companyName'])
            .wait(1000)

        manageProfilePageElements.getCompanyAddressField()
            .focus()
            .clear()
            .type(updateDetailsObj['companyAddress'])
            .wait(1000)    

        manageProfilePageElements.getCountryField()
            .focus()
            .clear()
            .type(updateDetailsObj['country'])
            .wait(1000)

        //press Enter
        manageProfilePageElements.getCountryField().type('{enter}').wait(1000)

        manageProfilePageElements.getCityField()
            .focus()
            .clear()
            .type(updateDetailsObj['city'])
            .wait(1000)    
           
        //press Enter
        manageProfilePageElements.getCityField().type('{enter}').wait(1000)    

        manageProfilePageElements.getAreaField()
            .focus()
            .clear()
            .type(updateDetailsObj['area'])
            .wait(1000)    

        manageProfilePageElements.getZipCodeField()
            .focus()
            .clear()
            .type(updateDetailsObj['zipCode'])
            .wait(1000)
           
        //iframe
        const iframe = manageProfilePageElements.getServiceDetailsField()
            .its('0.contentDocument.body')
            .should('be.visible')
            .then(cy.wrap)

        iframe.clear().type(updateDetailsObj['serviceDetails']).wait(1000)   

        manageProfilePageElements.getTermsAndConditionsField()
            .focus()
            .clear()
            .type(updateDetailsObj['termsAndConditions'])
            .wait(1000)    

        //imageandVideo
        manageProfilePageElements.getUploadImagesAndVideosField()
            .eq(1)
            .attachFile(updateDetailsObj['uploadImagesAndVideosFileName'])
            .wait(1000)

        manageProfilePageElements.getMapLocationField()
            .focus()
            .clear()
            .type(updateDetailsObj['mapLocation'])    
            .wait(1000)

        manageProfilePageElements.getPriceField()
            .focus()
            .clear()
            .type(updateDetailsObj['price'])
            .wait(1000)

        manageProfilePageElements.getDiscountField()
            .focus()
            .clear()
            .type(updateDetailsObj['discount'])
            .wait(1000)    
        
        manageProfilePageElements.getDiscountDateField()
            .focus()    
            .clear()
            .type(updateDetailsObj['discountDate'])
            .wait(1000)

        //press Enter
        manageProfilePageElements.getDiscountDateField().type('{enter}').wait(1000)
            
        manageProfilePageElements.getDiscountDetailsField()
            .focus() 
            .clear()
            .type(updateDetailsObj['discountDetails'])
            .wait(1000)     

        ///Upload Button
        manageProfilePageElements.getUploadButton().should('contain', updateDetailsObj['UpdateButtonName']).click().wait(1000)

    }

    updatePassword(updatePasswordObj){
        manageProfilePageElements.getCurrentPasswordField().clear().type(updatePasswordObj['currentPassword'])
        cy.wait(1000)
        manageProfilePageElements.getNewPasswordField().clear().type(updatePasswordObj['newPassword'])
        cy.wait(1000)
        manageProfilePageElements.getConfirmPasswordField().clear().type(updatePasswordObj['confirmPassword'])
        cy.wait(1000)
        
        ///Upload Button
        manageProfilePageElements.getUploadButton().should('contain', updatePasswordObj['UpdateButtonName']).click().wait(1000)
    }


}