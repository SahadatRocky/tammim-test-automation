/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class QuotationsPageElements{
   

    getdownloadButton(){
        return cy.get('a[download="Qutation-report.csv"]')
    }

    getQuotationAmount(){
        return cy.get('input[id="control-hooks_price"]')
    }

    getQuotationDate(){
        return cy.get('input[id="control-hooks_quotation_date"]')
    }

    getQuotationStartTime(){
        return cy.get('input[id="control-hooks_quotation_start_time"]')
    }

    getQuotationEndTime(){
        return cy.get('input[id="control-hooks_quotation_end_time"]')
    }

    getQuotationMessage(){
        return cy.get('textarea[id="control-hooks_message"]')
    }

    getReSubmitButton(){
       return cy.get('button.custom-btn')
    }

    getDeclineButton(){
        return cy.get('button.custom-btn-outline')
    }




}