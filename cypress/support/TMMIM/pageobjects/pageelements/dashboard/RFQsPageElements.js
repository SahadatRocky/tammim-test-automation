/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class RFQsPageElements{
   
   
    getMakeAQuotationButton(){
        return cy.get('button.custom-btn')
    }


    getDeclineButton(){
        return cy.get('button.custom-btn-outline')
        
    }

    //make a Quotations
    getMakeQuotationAmount(){
        return cy.get('input[id="control-hooks_price"]')
    }

    getMakeQuotationDate(){
        return cy.get('input[id="control-hooks_date"]')
    }

    getMakeQuotationStartTime(){
        return cy.get('input[id="control-hooks_start_time"]')
    }

    getMakeQuotationEndTime(){
        return cy.get('input[id="control-hooks_end_time"]')
    }

    getMakeQuotationMessage(){
        return cy.get('textarea[id="control-hooks_message"]')
    }

    getMakeChooseFile(){
        return cy.get('input[type="file"]')
    }

    getMakeSubmitButton(){
       return cy.get('button.custom-btn')
    }

    
    

}