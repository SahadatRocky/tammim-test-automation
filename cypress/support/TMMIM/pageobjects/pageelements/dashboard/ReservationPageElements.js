/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class ReservationPageElements{
   

    //new Reservation
    getNameField(){
         return cy.get('input[id="control-hooks_name"]')
    }

    getEmailField(){
        return cy.get('input[id="control-hooks_email"]')
    }

    getPhoneNumberField(){
        return cy.get('input[id="control-hooks_phoneNumber"]')
    }

    getDateField(){
        return cy.get('input[id="control-hooks_date"]')
    }

    getStartTimeField(){
        return cy.get('input[placeholder="Start time"]')
    }

    getEndTimeField(){
        return cy.get('input[placeholder="End time"]')
    }

    getAllDayField(){
        return cy.get('input[id="control-hooks_allDay"]')
    }

    getDepositAmountField(){
        return cy.get('input[id="control-hooks_deposite"]')
    }

    getTotalField(){
        return cy.get('input[id="control-hooks_total"]')
    }

    getCreateButton(){
        return cy.get('button.custom-btn')
    }

    getButtonClose(){
        return cy.get('button.close')
    }


    //listView 

    getCreateNewReservationButton(){
        return cy.get('div.select-status button[type="button"] span')
    }

    getReservationSelectOptionbyName(){
    return cy.get('select')
    }


    getContinueButton(){
        return cy.get('button.swal2-confirm')
    }

    getdownloadButton(){
        return cy.get('a[download="Reservation-report.csv"]')
    }

}