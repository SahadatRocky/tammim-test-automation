/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class SubscriptionPageElements{
   
    ///Subscribe
    getPlanField(){
        return cy.get('input[id="plan"]')
    }

    getProcessButton(){
        return cy.get('button.custom-btn')
    }

    ////Payment Details
    getCardNumberField(){
        return cy.get('input[id="number"]')
    }

    getExpireMonthField(){
        return cy.get('input[id="expmonth"]')
    }

    getExpireYearField(){
        return cy.get('input[id="expyear"]')
    }

    getCVVField(){
        return cy.get('input[id="cvv"]')
    }

    getLabelAmount(){
        return cy.get('div[id="pt_label_amount"]')
    }

    getPayNowButton(){
        return cy.get('button[id="payBtn"]')
    }

    getAuthenticateButton(){
        return cy.get('input[value="Authenticated"]')
    }

    ///paymentSuccessfully
    getContinueButton(){
        return cy.get('button.swal2-confirm')
    }
}