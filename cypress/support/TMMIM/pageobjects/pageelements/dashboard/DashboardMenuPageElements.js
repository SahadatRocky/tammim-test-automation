/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class DashboardMenuPageElements{
    getDashboard() 
    {
        return cy.get('a[href="/dashboard"]')
    }

    //Subscription
    getSubscription(){
        return cy.get('a.feat-btn')
    }

    getSubscribe(){
        return cy.get('a[href="/dashboard/subscribe"]')
    }

    getPaymentHistory(){
        return cy.get('a[href="/dashboard/payment-history"]')
    }

    //ManageProfile
    getManageProfile(){
        return cy.get('a.serv-btn')
    }

    getUpdateDetails(){
        return cy.get('a[href="/dashboard/update-details"]')
    }

    getUpdatePassword(){
        return cy.get('a[href*="/dashboard/update-password"]')
    }

    //Reservation
    getReservation(){
        return cy.get('a[href="/dashboard/reservation"]')
    }

    //Quotations
    getQuotations(){
        return cy.get('a[href="/dashboard/quotation"]')
    }

    getRFQs(){
        return cy.get('a[href="/dashboard/rfqs"]')
    }

    //Review
    getReview(){
        return cy.get('a[href="/dashboard/review"]')
    }

}