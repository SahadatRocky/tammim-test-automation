/// <reference types="cypress" />

const or = require('../../../locators/tmmim_locators.json');
export default class ManageProfilePageElements{
   
   //UpdateDetails

    getCompanyNameField() 
    {
        return cy.get('input[id="control-hooks_company_name"]')
    }

    getCompanyAddressField(){
        return cy.get('input[id="control-hooks_company_address"]')
    }

    getCountryField(){
        return cy.get('input[id="country"]')
    }

    getCityField(){
        return cy.get('input[id="city"]')
    }
   
    getAreaField(){
        return cy.get('input[id="control-hooks_area"]')
    }

    getZipCodeField(){
        return cy.get('input[id="control-hooks_zip_code"]')
    }

    ///iframe
    getServiceDetailsField(){
      return cy.get('iframe[id*="tiny-react_"]')
    }

    getTermsAndConditionsField(){
        return cy.get('textarea[id="control-hooks_terms_and_conditions"]')
    }

    getUploadImagesAndVideosField(){
        return cy.get('input[type="file"]')
    }

    getMapLocationField(){
        return cy.get('input[id="control-hooks_map_location"]')
    }

    getPriceField(){
        return cy.get('input[id="control-hooks_price"]')
    }

    getDiscountField(){
        return cy.get('input[id="control-hooks_discount_amount"]')
    }

    getDiscountDateField(){
        return cy.get('input[id="control-hooks_discount_end_date"]')
    }

    getDiscountDetailsField(){
        return cy.get('textarea[id="control-hooks_discount_details"]')
    }  

    ///Upload Button

    getUploadButton(){
        return cy.get('button.custom-btn')
    }

    
    ///////////// Update Password

    getCurrentPasswordField(){
        return cy.get('input[id="control-hooks_current_password"]')
    }

    getNewPasswordField(){
        return cy.get('input[id="control-hooks_new_password"]')
    }

    getConfirmPasswordField(){
        return cy.get('input[id="control-hooks_confirm_password"]')
    }

}