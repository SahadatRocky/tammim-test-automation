// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.

require('@4tw/cypress-drag-drop')
import 'cypress-file-upload';
require('cypress-downloadfile/lib/downloadFileCommand')

// Import pages 
// ***********************************************
//Common page import
import LoginPage from '../support/commonPage/LoginPage'
import DashboardLogoutPageElements from '../support/TMMIM/pageobjects/pageelements/dashboard/DashboardLogoutPageElements'
//-- This is the create page object --
// ***********************************************
//Common create page object 
const loginPage = new LoginPage()
const dashboardLogoutPageElements = new DashboardLogoutPageElements()

// -- This is Login Method SDC --
Cypress.Commands.add("login", (email, password) => 
{

    loginPage.getEmailField().type(email).should('have.value', email)
    cy.wait(1000)
    loginPage.getPasswordField().type(password).should('have.value', password)
    cy.wait(1000)
    loginPage.getRememberMe().click()
    cy.wait(1000)
    loginPage.getLoginButton().should('contain', "Login").click()
    cy.wait(2000)
})


// -- This is Logout Method QPE --
Cypress.Commands.add("logout", () => 
{
    dashboardLogoutPageElements.getLogoutButton().click().wait(1000)
  
})



//Pagination
/*
Cypress.Commands.add("pagination", (pageItemNo) => 
{
    requisitionDeclarationPage.getPaginationDropDownField().click()
    cy.wait(1000)        
    requisitionDeclarationPage.getPaginationDropDownFieldValue().contains(pageItemNo).click()
    cy.wait(5000)
})
*/

//Calendar Method
/*
Cypress.Commands.add("calendar", (year, month, day) => 
{
    requisitionDeclarationPage.getCalendarYearView().click()
    cy.wait(1000)
    requisitionDeclarationPage.getYearMonthDate().contains(year).click()
    cy.wait(1000)
    requisitionDeclarationPage.getYearMonthDate().contains(month).click()
    cy.wait(1000)
    requisitionDeclarationPage.getYearMonthDate().contains(day).click()
    cy.wait(1000)
})
*/


//For visible items
/*
Cypress.Commands.add("visibleItems", (element) => 
{
    receiveGoodsPage.getCardRowsvisible().each(($el, index, $list) =>     
    {
        const textDescription=$el.find('td.e-rowcell[aria-label]').text()
        if(textDescription.includes(element))                    
        {
            $el.find('td button mat-icon').click()
        }
    })
})
*/

//Select the desired items checkbox
/* 
Cypress.Commands.add("selectItems", (element) => 
{
    receiveGoodsPage.getCardRows().each(($el, index, $list) =>    
    {
        const textAssetTagNo=$el.find('td.e-rowcell[aria-label]').text()
        if(textAssetTagNo.includes(element))                    
        {
            $el.find('.e-templatecell label span.e-icons').click()
        }
    })
})
*/
//Pagination
/*
Cypress.Commands.add("ASTPagination", (pageItemNo) => 
{
    receiveGoodsPage.getPaginationDropDownField().click()
    cy.wait(1000)        
    receiveGoodsPage.getPaginationDropDownFieldValue().contains(pageItemNo).click()
    cy.wait(5000)
})
*/
