/// <reference types="cypress" />
// const or = require('../../../locators/dp_locators.json');
class LoginPage
{
 
    ///Email Field
    getEmailField(){
       return cy.get('input[id="control-hooks_email"]');
    }

    ///Password Field
     getPasswordField(){
        return cy.get('input[id="control-hooks_password"]');
     }

     getRememberMe(){
        return cy.get('input.option-input.checkbox');
     }

     getLoginButton(){
        return cy.get('button.login-btn')
     }

}
export default LoginPage;