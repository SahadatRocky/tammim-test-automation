/// <reference types="cypress" />
const or = require('../../support/TMMIM/locators/tmmim_locators.json')
//const common = require('../../../support/TMMIM/locators/common_locators.json')
import CommonPageActions from '../../support/TMMIM/pageobjects/pageactions/common/CommonPageActions'
import ManageProfilePageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/ManageProfilePageActions'
import DashboardMenuPageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/DashboardMenuPageActions'
import SubscriptionPageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/SubscriptionPageActions'
import RFQsPageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/RFQsPageActions'
import QuotationsPageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/QuotationsPageActions'
import ReservationPageActions from '../../support/TMMIM/pageobjects/pageactions/dashboard/ReservationPageActions'

const commonPage = new CommonPageActions()
const manageProfilePageActions = new ManageProfilePageActions()  
const dashboardMenuPageActions = new DashboardMenuPageActions()
const subscriptionPageActions = new SubscriptionPageActions()
const rfqsPageActions= new RFQsPageActions() 
const quotationsPageActions = new QuotationsPageActions()
const reservationPageActions = new ReservationPageActions()

    before(()=>{
        cy.fixture('TMMIMTestData').then((data)=>{

            globalThis.data = data

        })

     

    })

    beforeEach(()=>{
    //cy.visit(Cypress.env('baseUrl'))

        cy.visit('/', {
            onBeforeLoad: win => {   
            win.sessionStorage.clear();
            }
          });   
    })

    let i = 0;   
    describe("Working TMMIM module",() => {

       it("TC_1. PE: login TMMIM, Update Profile Details and logout",()=>{   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuManageProfileUpdateDetails()
            cy.wait(1000)
            manageProfilePageActions.updateProfileDetails(data.TMMIM.dashboard.manageProfile.updateDetails)
            cy.wait(1000)
       })
       
       it.skip("TC_2. PE: login TMMIM, Update password and logout",()=>{   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuManageProfileUpdatePassword()
            cy.wait(1000)
            manageProfilePageActions.updatePassword(data.TMMIM.dashboard.manageProfile.updatePassword)
            cy.wait(1000)
       })

       it("TC_3. PE: login TMMIM,Subscription subscribe Plan and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuSubscriptionSubscribe()
            cy.wait(1000)
            subscriptionPageActions.subscribePlan(data.TMMIM.dashboard.subscription.subscribe)
            cy.wait(1000)
        })

        it("TC_4. PE: login TMMIM, check Subscription payment history and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuSubscriptionPaymentHistory()
            cy.wait(1000)
            //subscriptionPageActions.checkPaymentHistory(data.TMMIM.dashboard.subscription.paymentHistory)
            //cy.wait(1000)
        })

        it("TC_5. PE: login TMMIM,RFQs- RFQs No. Search and make a Quotation and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuRFQs()
            cy.wait(2000)

            ///close
            rfqsPageActions.RFQsSelectOptionbyName(data.TMMIM.dashboard.RFQs.selectOptions.Close)
            cy.wait(2000)
            ///active
            rfqsPageActions.RFQsSelectOptionbyName(data.TMMIM.dashboard.RFQs.selectOptions.Active)
            cy.wait(2000)

            rfqsPageActions.SearchRFQsNoAndClick(data.TMMIM.dashboard.RFQs)
            cy.wait(1000)
            rfqsPageActions.MakeAQuotationButton(data.TMMIM.dashboard.RFQs)
            cy.wait(1000)
        })

        it("TC_6. PE: login TMMIM,Quotation Download CSV file and RFQs No. Search and resubmit Quotation and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)    
            dashboardMenuPageActions.onClickMenuQuotations()
            cy.wait(2000)
            ///only support https url (href) 
            //quotationsPageActions.DownloadQuotationCSVFile(data.TMMIM.dashboard.Quotations)
            //cy.wait(1000)
            quotationsPageActions.ClickDownloadQuotationCSVFile(data.TMMIM.dashboard.Quotations)
            cy.wait(2000)

            ///Pending
            quotationsPageActions.QuotationSelectOptionbyName(data.TMMIM.dashboard.Quotations.selectOptions.Pending)
            cy.wait(2000)
            ///Closed
            quotationsPageActions.QuotationSelectOptionbyName(data.TMMIM.dashboard.Quotations.selectOptions.Closed)
            cy.wait(2000)
            ///Denied
            quotationsPageActions.QuotationSelectOptionbyName(data.TMMIM.dashboard.Quotations.selectOptions.Denied)
            cy.wait(2000)
            ///Accepted
            quotationsPageActions.QuotationSelectOptionbyName(data.TMMIM.dashboard.Quotations.selectOptions.Accepted)
            cy.wait(2000)

            quotationsPageActions.SearchRFQsAndClick(data.TMMIM.dashboard.RFQs)
            cy.wait(1000)
            quotationsPageActions.ResubmitQuotation(data.TMMIM.dashboard.Quotations)
            cy.wait(1000)
        })


        it("TC_7. PE: login TMMIM,Reservation calendar view create new Reservation and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)
            dashboardMenuPageActions.onClickMenuReservation()
            cy.wait(2000)
         
            reservationPageActions.SearchRFQsWithSpecificDateAndClicked(data.TMMIM.dashboard.Reservation.calenderView)
            reservationPageActions.CalenderViewCreateReservation(data.TMMIM.dashboard.Reservation.createReservation)

        })

        it("TC_8. PE: login TMMIM,Reservation list view create new Reservation and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)
            dashboardMenuPageActions.onClickMenuReservation()
            cy.wait(2000)
            reservationPageActions.ReservationSelectOptionbyName(data.TMMIM.dashboard.Reservation.selectOption.listView)
            cy.wait(2000)

            reservationPageActions.ClickDownloadReservationCSVFile(data.TMMIM.dashboard.Reservation.listView)
            cy.wait(2000)

            //click create reservation button
            reservationPageActions.CreateNewReservationButton(data.TMMIM.dashboard.Reservation.listView)
            cy.wait(1000)
            reservationPageActions.ListViewCreateReservation(data.TMMIM.dashboard.Reservation.createReservation)
            cy.wait(1000)
        })

        it("TC_9. PE: login TMMIM,Review & Rating and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)
            dashboardMenuPageActions.onClickMenuReview()
            cy.wait(3000)
        })

        it("TC_10. PE: login TMMIM,Dashboard and logout",()=> {   
         
            cy.login(data.validLoginCredentials.TMMIM.credentials[i].email,data.validLoginCredentials.TMMIM.credentials[i].password)
            dashboardMenuPageActions.onClickMenuDashboard()
            cy.wait(3000)
        })
    })
    
    afterEach(() => {
        Cypress.on('fail', (error, runnable) => {
            cy.log('this single test failed, but continue other tests');
            // don't stop!
            // throw error; // marks test as failed but also makes Cypress stop
        });
        cy.logout()
    }) 
      